.model      small  

;������
.data     
    result_msg      db      "Result:$"  
    input_msg       db      "Enter string:$"
    string          db      200, 200  dup('$') 
  
    

;����
.stack dw 200 dup(0)

;���
.code
start:
	mov ax, data            ;\
    mov ds, ax              ; > �������� �������� ������
    mov es, ax              ;/

inputString:     
    lea dx, input_msg       ;\
    mov ah, 09h             ; >  ����� ��������� �� �����
    int 21h                 ;/       
        
    mov ah, 0Ah             ;\                                  
    lea dx, string          ; >  ���������� ������ � ���������� 
    int 21h                 ;/     
        
    call printEndline       ; ��������� ������� ������� ����� ������ 
    
    call strLen             ;\    
    cmp al, 0               ; > �������� �� ������� ����� �������� ������
    jz inputString          ;/
    
    
    call strLen
    xor cx,cx    
    mov cl,al
    lea bx,string
    mov si,2                ;����� ������� ������� � ������ 
main_loop:
	cmp cx,0
    jz printResult
    call findNextWord
    call findLastCharInWord
    call revertWord

    loop main_loop               

quit:    
    mov ax, 4c00h           ;����� �� ���������
    int 21h 
    
printResult:
    lea dx, result_msg      ;\
    mov ah, 09h               ; >  ����� ��������� �� �����
    int 21h                 ;/

    mov ah, 09h             ;\
    lea dx, string          ; > ����� �������������� ������
    add dx, 2
    int 21h                 ;/ 
    jmp quit
        
;-----------------------------------------------------------------
; ��������� ������ ����� ������ (CR+LF)
printEndline proc near
    push ax             ;���������� ���������
    push dx  
    
    mov ah,2            ;������� DOS 02h - ����� �������
    mov dl,13           ;������ CR
    int 21h

    mov dl,10           ;������ LF
    int 21h 
    
    pop dx              ;�������������� ���������
    pop ax 
    ret                 ;������� �� ���������
printEndline endp

;-----------------------------------------------------------------
; ������������ ����� ������  
;
; ����: dx - ����� ������
; 
; �����: al - ����� ������
strLen proc near 
    push si
    
    mov si, dx
    inc si
    mov al, [si] 
    
    pop si 
    ret
strLen endp 

;-----------------------------------------------------------------
; ���������� ���������� �����
;
; ����: bx - ����� ������, si - ����� �������� �������
; 
; �����: si - ����� ������� ������� ���������� �����
findNextWord proc near
    push ax

loop1:
    mov al, [bx+si]

    cmp al, ' ' ;ASCII-��� �������
    jz increase1

    pop ax
    ret

increase1:
    inc si
    jmp loop1
    
findNextWord endp

;-----------------------------------------------------------------
; ���������� ���������� ������� �����
;
; ����: bx - ����� ������, si - ����� ������� ������� �����
; 
; �����: di - ����� ���������� ������� �������� �����
findLastCharInWord proc near
    push ax
    push dx
    push cx
    
    lea dx, string
    xor ax,ax
    call strLen
    add al,2
    cmp si, ax
    jz printResult
    
    mov di, si

loop2:
	cmp di,ax
	jz loop2_end
	
    mov cl, [bx+di]

    cmp cl, ' '
    jnz increase2
    jz loop2_end

loop2_end:
	pop cx
	pop dx
    pop ax
    dec di ;� di ����� ����� ������� ����� �����, ������� ������� 1
    ret

increase2:
    inc di
    jmp loop2
    
findLastCharInWord endp

;-----------------------------------------------------------------
; ������ �������� �����
;
; ����: bx - ����� ������, si - ����� ������� ������� �����, di - ����� ���������� ������� �������� �����
; 
; �����: bx - ������������� ������
revertWord proc near
    push ax
    push dx
    push cx
    push di
   
    mov ax, di
    sub ax, si
    mov cx, 2
    div cl ;����� ���������� � a� = (di-si)/2
    mov cl, al
    add cl, ah ;��������� ������� �� �������
    
loop3:
    mov al, [bx+si] 
    mov dl, [bx+di]

    mov [bx+si], dl
    mov [bx+di], al
    inc si
    dec di

    loop loop3
    
    pop si
    inc si ;����� �� ������� �����
    pop cx
    pop ax
    pop dx
    ret
	

revertWord endp

end start