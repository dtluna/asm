.model small

.stack
    dw 200h dup(0)

.data
    notation1_str       db 5 dup('$')
    notation2_str       db 5 dup('$')
    notation1_num       dw 0
    notation2_num       dw 0
    target_number_str   db 20 dup('$')
    target_number       dw 0
    result              db 30 dup('$')
    enter               db 10,13,'$'
    invite1             db "Enter number notation: $"
    invite2             db "Enter number: $"
    invite3             db "Enter target number notation: $"
    result_message      db "Result: $"
    error1              db "Invalid notation. Entering error or out of range (2-16)",10,13,'$'
    error2              db "Number is not specified radix",10,13,'$'
    pkey                db "Press any key for exit...$"

.code
    start:
        mov     ax, data                ; �������� ������ ������ � ���������� ��������
        mov     ds, ax
        mov     es, ax
    
    input_str macro string, number      ; ������ ����� ������ �������� ������
        
        lea     dx, string
        mov     bx, dx
        mov     [bx], number
        mov     ah, 0Ah
        int     21h 
    endm
    
    out_str macro string                ; ������ ������ ������
             
        lea     dx, string
        mov     ah, 09h
        int     21h 
    endm
    
    endline macro                       ; ������ �������� �������
        
        lea     dx, enter
        mov     ah, 09h
        int     21h
    endm
              
    entering_notation1:

        out_str     invite1             ; ������� �����������
        input_str   notation1_str, 3    ; ������ ��������� ������� ��������� �������
        endline
        lea         dx, notation1_str
        call        check_number        ; 0 � �� � ������ ������ �����
        cmp         ax, 0
        je          notation1_error
        call        str_to_num
        cmp         ax, 2
        jl          notation1_error
        cmp         ax, 16
        jg          notation1_error
        mov         notation1_num, ax   ; �������� ����� � notation1_num     
            
    entering_number:
    
        out_str     invite2
        input_str   target_number_str, 20
        endline
        lea         dx, target_number_str
        mov         ax, notation1_num
        call        check_radix
        cmp         ax, -1
        je          number_entering_error
        lea         dx, target_number_str
        mov         ax, notation1_num
        call        convert_to_decimal
        mov         target_number, ax

    entering_notation2:
    
        out_str     invite3
        input_str   notation2_str, 3
        endline
        lea         dx, notation2_str
        call        check_number        ; 0 � �� � ������ ������ �����
        cmp         ax, 0
        je          notation2_error
        call        str_to_num
        cmp         ax, 2
        jl          notation2_error
        cmp         ax, 16
        jg          notation2_error
        mov         notation2_num, ax
        
        mov         dx, target_number
        mov         ax, notation2_num
        call        convert_number
        out_str     result_message
        out_str     result
        endline
        
        jmp         end_programm
        
    notation1_error:
    
        out_str     error1
        jmp         entering_notation1
        
    notation2_error:
    
        out_str     error1
        jmp         entering_notation2
    
    number_entering_error:
    
        out_str     error2
        jmp         entering_number
       
    end_programm:  
          
        out_str pkey              
        mov     ah, 01h
        int     21h   
        mov     ah, 4ch
        int     21h
    
    ; ------------------------------ ��������� ---------------------------------------    
    
    ; ������� ������ � ����� (�������� ������ - dx, ��������� � ax)
    
    str_to_num proc         
        
        push    cx
        push    bx
        push    di
        push    si                   
        
        xor     cx, cx
        xor     bx, bx
        xor     ax, ax
        xor     di, di
        
        mov     si, dx
        mov     cl, [si + 1]
        mov     bx, 1
        add     si, 2                   ; ����������� �������� ��������� 2 ������ ����� � ����� � ������
        mov     al, [si]
        cmp     al, '-'                 ; ���� ������ ������ - ���� �����, ��������� ���������� ������ ��� �������� 
        je      negative1               ; �� ������ � ����� �� 1 (�� ���������� �����)
        continue_negative:
        add     si, cx                  ; ����������� �������� �� ������� ����� ������
        dec     si                      ; ��������� �� ��������� ������
        
        loop1:
        
        mov     al, [si]                ; ���������� ������ 
        mov     ah, '0'                 ; �������� ASCII '0'
        sub     al, ah
        xor     ah, ah                  
        imul    bx                      ; �������� ax �� ������� 10 
        
        push    ax                      ; �������� dx �� 10
        mov     ax, bx
        mov     bx, 10
        mul     bx
        mov     bx, ax
        pop     ax
        add     di, ax                  ; ��������� �������� ���������� �� ������� 10
        
        dec     si         
        
        loop loop1
        
        mov     ax, di                  ; ���������� ���������� ��������� � ax
        xor     cx, cx                  
        mov     cl, [si]                
        cmp     cl, '-'                 ; ���� ������ ������ ���� ��� ������� - ������ ���� ����� � ax
        je      negative2
        
        pop     si
        pop     di
        pop     bx
        pop     cx                    
        ret
        
        negative1:
        
        dec     cx                      ; ��������� ������ ������ ��� �� �� ��������� ����� ��� ��������
        inc     si                      ; ������������ ���������� ������ ������
        jmp     continue_negative
        
        negative2:
        
        neg     ax                      ; ������ ���� ax      
        pop     si
        pop     di
        pop     bx
        pop     cx 
        ret 
    endp
    
    ; ��������� ����� � ������ �� ������������ ( �������� ������ � dx, 0 � ax - ������ )  
    
    check_number proc                   
        
        push    cx
        push    si
        
        mov     si, dx                  ; �������� �������� � si ��� ��������������
        xor     cx, cx
        mov     cl, [si + 1]            ; ������ ������ � cl
        add     si, 2                   ; ������������� �� ������ �����
        mov     al, [si]                
        cmp     al, '-'                 ; ���� ����� ������ ����� ���� ����� - ��������� �� 1 ���� �����
        je      negative_check         
        
        loop_check:                     ; ��� ��������� �������� ������ ���� ������ "0" � ������ "9"
        
        mov     al, [si]                
        cmp     al, '0'
        jl      error_check
        cmp     al, '9'
        jg      error_check
        inc     si
        
        loop    loop_check 
        
        pop     si
        pop     cx
        ret
        
        negative_check:
         
        dec     cl 
        inc     si
        jmp     loop_check
        
        error_check:                    ; � ������ ������ ax = 0
        
        xor     ax, ax
        pop     si
        pop     cx
        ret
        
    endp
    
    ; �������� ����� ��������������� �������� (�������� � dx) �� ������������� 
    ; ��������� ������� ��������� (ax)
    ; � ������ ������ - (ax = -1)    
    
    check_radix proc
        
        push    cx
        push    bx
        push    si
        push    di                    
        
        xor     cx, cx
        xor     bx, bx
         
        mov     si, dx                  ; ���������� �������� � si ��� ��������������
        mov     di, ax                  ; ���������� ��������� ������� ��������� � di
        mov     cl, [si + 1]            ; ������ ������ � cl
        cmp     cl, 0
        je      check_radix_error
        add     si, 2                   ; ��������� �� ������ ������ � ������
        xor     ax, ax
        mov     bl, [si]                ; ��������� ������ ������� �� ���� -, ���� �� ���� - ��������� � ���������� �������
        cmp     bl, '-'
        je      negative_handler     
        
        loop_check_radix:
        
        mov     al, [si]                ; ��������� ������
        call    character_to_digit      ; ��������� ������ � �����, ��� ������ ax = -1 
        cmp     ax, -1
        je      check_radix_error
        cmp     ax, di                  ; ���� ����� ������ ��� ����� ��������� ������� ��������� - ��� ���� ������
        jge     check_radix_error
        inc     si        
        
        loop loop_check_radix 
        
        pop     di
        pop     si
        pop     bx
        pop     cx
        
        ret
        
        negative_handler:
        
        inc     si
        dec     cl
        jmp     loop_check_radix
        
        check_radix_error:
        
        mov     ax, -1
        
        pop     di
        pop     si
        pop     bx
        pop     cx
        
        ret
        
        
    endp
    
    ; ��������� ���������� ����� �� ������� 
    ;(������� ������ - ������ � ax, �������� - ����� � ax, -1 ��� ������)
    
    character_to_digit proc              
        
        push    cx
        push    dx
        
        xor     dx, dx
        mov     cx, 10
        mov     ah, '0'                 
        
        loop_gdd:
        
        cmp     ah, al                  ; ���������� ASCII ����� � ��� ��� ����� � al
        je      set_value               ; ���� ����� - ���������� � �� �� �������� ��� ���������� � dx
        inc     ah                      ; ����������� �������� ASCII ����
        inc     dx                      ; ����������� �������� ��������
         
        loop loop_gdd
        
        cmp     al, 'A'
        je      set_value
        inc     dx
        cmp     al, 'B'
        je      set_value
        inc     dx
        cmp     al, 'C'
        je      set_value
        inc     dx
        cmp     al, 'D'
        je      set_value
        inc     dx
        cmp     al, 'E'
        je      set_value
        inc     dx
        cmp     al, 'F'
        je      set_value
        mov     ax, -1                  ; ���� ���������� ��� - ������
        pop     dx
        pop     cx
        ret
          
        set_value:
        
        mov     ax, dx
        pop     dx
        pop     cx 
        ret
    endp
    
    ; ������� ����� ��������� �������� (�������� � dx) 
    ; � ���������� ������� ��������� (ax)
    ; � ���������� ������� ���������
    ; � ������ ������ � ax - ���������� �����
     
    convert_to_decimal proc
        
        push    bx
        push    cx
        push    di
        push    si
        
        xor     bx, bx
        xor     cx, cx
        xor     di, di
        
        mov     si, dx                  ; �������� ������ � ������ � si ��� ��������������
        mov     di, ax                  ; ��������� ������� ��������� � di
        mov     dx, 1                   ; ��� ������� � dx
        xor     ax, ax
        mov     cl, [si + 1]            ; ������ ������ � cx
        add     si, 2                   ; ��������� �� ������� ������� �������
        mov     al, [si]
        cmp     al, '-'
        je      transfer_negative1
        transfer_continue:
        add     si, cx                  ; ��������� � ����� ������
        dec     si
        
        transfer_loop:
        
        mov     al, [si]
        call    character_to_digit      ; � ax - �������� �������
        push    dx                      ; ��������� dx � ����
        mul     dx                      ; �������� �������� ������� �� ��� ���
        pop     dx                      ; ���������� ������ �������� dx �� �����
        push    ax                      ; ��������� ���������� � ����
        mov     ax, dx                  ; ax = dx
        mul     di                      ; ax *= di, dx -> 0
        mov     dx, ax                  ; dx = ax
        pop     ax                      ; ���������� ax ������ ��������
        add     bx, ax                  ; ����������� ��������� � �������� bx
        dec     si
        
        loop    transfer_loop
        
        mov     al, [si]                ; �� ��������� ��������� ��������� ����� �� ���������������
        cmp     al, '-'
        je      transfer_negative2      
        mov     ax, bx      
        
        pop     si
        pop     di
        pop     cx
        pop     bx
        ret
       
        transfer_negative1:
        
        inc     si
        dec     cx
        jmp     transfer_continue
        
        transfer_negative2:
        
        mov     ax, bx
        neg     ax                      ; � ������ ���� ����� ������������� - ������ ����
        
        pop     si
        pop     di
        pop     cx
        pop     bx 
        ret
           
    endp
    
    ; � ax - �����, �� ������ � ax - ASCII ���
    ; � ������ ������ - ax = -1
    
    digit_to_character proc
        
        push    cx
        push    dx
        
        cmp     ax, 0                   ; ���� ����� ������ 0 - ������
        jl      dtc_error               
        cmp     ax, 15                  ; ���� ����� ������ 15 - ������
        jg      dtc_error
        
        xor     dx, dx                  ; ������� ��������
        mov     cx, 16                  ; ������� �����
        
        dtc_loop:
        
        cmp     al, dl                  ; ���������� �������� � al �� ��������� ��������
        je      set_character           ; ���� ����� - ������������� ������ � ax
        inc     dl
        
        loop dtc_loop
        
        jmp     dtc_error
        
        set_character:
        
        cmp     al, 9                   ; ���� ����� ������ 10 ������������� ����������� ������
        jg      set_spec_char
        add     dl, '0'                 ; ���� �� ������ 10 - ��������� ASCII '0' 
        mov     al, dl                  ; �������� ������ � al
        
        pop     dx
        pop     cx
        ret
        
        set_spec_char:
        
        sub     dl, 10                  ; 'F' = 15 - 10 + 'A'
        add     dl, 'A'
        mov     al, dl
        
        pop     dx
        pop     cx
        ret
        
        dtc_error:
        
        mov     ax, -1                  ; � ������ ������ - ax = -1
        
        pop     dx
        pop     cx
        ret
        
    endp
    
    ; ��������� �������� ����������� ����� ��������� � dx
    ; � ������� ��������� �������� � ax
    
    convert_number proc
        
        push    bx
        push    cx
        push    si
        push    di
        
        push    dx
        lea     dx, result              ; �������� ������� ���������� � si
        mov     si, dx                  
        pop     dx
        
        cmp     dx, 0
        je      zero
        jnl     converting
        
        
        mov     [si], '-'               ; ���� ����� ������ ���� ���������� � ������� '-' � ��������� ����� �� ����
        neg     dx                      ; ������ ������ ���� �����   
        inc     si
        
        converting:
        
        mov     bx, ax                  ; ��������� ������� ������� ��������� � bx
        mov     ax, dx                  ; ����� � ax ( ������� )
        xor     dx, dx                  ; �������� dx - ���� ��������� ������� �� �������
        mov     cx, 30                  ; ���������� �������� ����� � cx                  
        
        loop_converting:
        
        div     bx
        push    ax
        mov     ax, dx                  ; ������� � ax (�������� ���������)
        call    digit_to_character      ; ��������� ����� � ������ - ��������� � ax
        mov     [si], al                ; �������� ������ � �������
        inc     si
        pop     ax                      ; �������� �� ����� ������ �������� ax
        xor     dx, dx                  ; �������� �������
        cmp     ax, 0                   ; ���� ����� ��� ���������� - ������� �� �����
        
        loopne loop_converting
        
        ; �������� - ����� � ������ ��������� � �������� �������
        ; ������� ����� ����������� ������� ��� ���������� �������������
        
        lea     dx, result              ; �������� ������ ���������� ����� di
        mov     di, dx
        
        cmp     [di], '-'               ; ���� ������ ������� '-' ���������� ���
        jne     continue
        inc     di
        
        continue:
        
        mov     si, di                  ; si ��������� �� ������ ������ � ������������ ��� ����������� � ����� ������
        mov     cx, 30                  ; ������� �����
        
        loop_converting_2:              ; ������������� � ����� ������
        
        inc     si
        cmp     [si], '$'
        
        loopne loop_converting_2
        
        dec     si
        cmp     si, di
        je      quit
        mov     ax, si                  ; �������� � ax �������� ���������� ������� � ������
        sub     ax, di                  ; �������� �������� ������� �������
        inc     ax                      ; ����������� �� 1 ������� ������ ������
        xor     dx, dx
        mov     dl, 2
        div     dl                      ; ����� ������ ������ �� 2 ������� ���������� ����������� ������������
        mov     cl, al                  ; ���������� ����������� ����� � cx
        
        loop_inverting:
        
        mov     al, [si]
        mov     ah, [di]
        mov     [si], ah
        mov     [di], al
        dec     si
        inc     di
        
        loop loop_inverting
        
        quit:
        pop     di
        pop     si
        pop     cx
        pop     bx
        ret
        
        zero:
        
        mov     [si], '0'               ; ���������� ��� 0
        
        pop     di                      
        pop     si
        pop     cx
        pop     bx 
        ret
        
    endp 
    
end start
