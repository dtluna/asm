;������������ ������� ����� ����� (������ ������� 5�6 ���������).
;�������� ���������:
;-- ���������� ����� ��������� �������;
;-- ����� ����� (��������, ������������) ��������� ����� (��������);
;-- ����� ������ ����� (��������) � ������������ ������ (���������,
;   �������������) ���������;
;-- ������������� ������� �� ������������� �������� �� ���������;
;-- ������������� ������ (�������) �� ����������� (��������) ��������
;   ������������ ��������� (�������� ������������� � ������������ ��������);
;-- ���������� ������� � �������� �������� �������� 3*3 (5*5).

.model small

.stack
    dw 400h dup(0)

max_lines  = 3
max_rows   = 2
max_strlen  = 5
i_pos_input = 8
j_pos_input = 11
i_pos_output = 2
j_pos_output = 5
i_pos_line_sum = 5

.data 
    input_msg              db "Enter m[i][j]:$";8,11
    matrix_msg             db "m[i][j]:$";2,5
    line_sum_msg           db "Line[i] sum:$";5
    sum_msg                db "Matrix sum:$"
    size_error_msg         db "Invalid number length! Max: 5 digits, min: 1 digit.$"
    not_a_number_error_msg db "The string is not a number!$"
    value_error_msg        db "Invalid value: number is more than 32767 or less than -32768!$"
    newline                db 10,13,'$'
    i                      db 0
    j                      db 0

    number_string          db 7, 7 dup('$')
    string                 db 10, 10 dup('$');������������ ���������� �������� ��� ����� � 6, 
                                             ;���� ��� �� ������ ��� �����, �� newline ��� ���� � �� ������

    matrix                 dw max_lines*max_rows, max_lines*max_rows dup(0)

    sum                    dw 0
    line_sums              dw max_lines, max_lines dup(0)


matrix_sum macro matrix ;����� ������� ����� � sum
    local sum_loop
    push cx
    push ax
    push si

    mov cx, max_lines*max_rows
    mov si, 2

    sum_loop:
        mov ax, matrix[si]
        add sum, ax
        add si, 2
        loop sum_loop

    pop si
    pop ax
    pop cx
    endm

matrix_line_sum macro matrix
    local i_loop, j_loop
    push cx
    push ax
    push si
    push di

    mov cx, max_lines
    mov si, 2
    mov di, 2

    i_loop:
        push cx
        xor ax,ax
        mov cx, max_rows
        j_loop:
            add ax, matrix[si]
            add si,2
            loop j_loop

        mov line_sums[di], ax
        add di,2
        pop cx
        loop i_loop

    pop di
    pop si
    pop ax
    pop cx
    endm

print_input_msg macro i,j
    push ax

    mov al,i
    mov ah,j
    add al,'0'
    add ah,'0'
    mov input_msg[i_pos_input], al
    mov input_msg[j_pos_input], ah
    print_string input_msg

    pop ax
    endm

print_line_sum_msg macro i
    push ax

    mov al, i
    add al, '0'
    mov line_sum_msg[i_pos_line_sum], al
    print_string line_sum_msg

    pop ax
    endm

print_output_msg macro i,j
    push ax

    mov al,i
    mov ah,j
    add al,'0'
    add ah,'0'
    mov matrix_msg[i_pos_output], al
    mov matrix_msg[j_pos_output], ah
    print_string matrix_msg

    pop ax
    endm

print_matrix macro matrix
    local i_loop, j_loop 
    push bx
    push si
    push cx

    mov cx, max_lines ;��������� ���������� �����
    mov si,2 ;���������� �� ������ ������� �������
    xor bx,bx ; �������� i; � bl ����� i, � � bh � j
    i_loop: ;���� �������� �� �������
        push cx ;��������� ���������� ���������� ����������
        mov cx, max_rows ;��������� ���������� ��������
        xor bh,bh ;�������� j
        j_loop: ;���� �������� �� ��������
            mov i, bl 
            mov j, bh
            print_output_msg i,j
            print_number matrix[si]
            print_newline
            inc bh ;����������� j
            add si,2 ;��������� �� ��������� ������� � �������
            loop j_loop
        inc bl ;����������� i
        pop cx ;��������������� ���������� ����������
        loop i_loop

    pop cx
    pop si
    pop bx
    endm

enter_matrix macro matrix 
    local i_loop, j_loop
    push ax
    push bx; ������� ��������)))
    push si; ������ �����))))0
    push cx

    mov cx, max_lines
    mov si,2
    xor bx,bx
    i_loop:
        push cx
        mov cx, max_rows
        xor bh,bh
        j_loop:
            mov i, bl
            mov j, bh
            enter_number
            mov matrix[si], ax 
            inc bh
            add si,2
            loop j_loop
        inc bl
        pop cx
        loop i_loop
    
    pop cx
    pop si
    pop bx
    pop ax
    endm

enter_number macro ;� ax ����� �������� �����
    local enter_loop
    push dx

    enter_loop:
        call input_string
        call convert_to_number
        cmp dx, 1
        jz enter_loop

    pop dx
    endm

print_number macro number
    push ax
    push dx

    mov ax, number
    count_digits ax
    lea dx, number_string
    call convert_to_string
    print_string number_string

    pop dx
    pop ax
    endm

print_double macro number

    endm

count_digits macro number ;� cx ����� ����� ��������
    local count_digits_loop, quit, negative, negative_back, more 
    push bx
    push ax
    push dx
    push di
    
    mov bx, number
    cmp bx, 0
    js negative ;���� ����� <0
    negative_back:
        mov ax,9 ;����� ������� ����� � ����� ������
        mov cx,1 ;����� � ����� ��� ������� ����
        mov di,10 ;��������� ���������
        xor dx,dx ;��������
    count_digits_loop:
        cmp bx, ax ;���� ���� ����� ������ ������ �������� ����� � ������ ����������� ����   
        ja more
        jna quit

    negative:
        neg bx ;������ �������������
        jmp negative_back

    more:
        inc cx ;����������� ������� ����
        mul di   ; | ����������� ������ �������
        add ax,9 ;/
        jmp count_digits_loop
        
    quit:
        pop di
        pop dx
        pop ax    
        pop bx
        endm

str_len macro string, reg ;� reg ����� ����� ������
    push bx
    
    lea bx, string
    inc bx
    mov reg, [bx]
    
    pop bx
    endm

enter_string macro string
    push ax
    push dx
    
    lea dx, string
    mov ah, 0Ah
    int 21h
    
    pop dx
    pop ax
    endm

print_user_string macro string
    push ax
    push dx

    lea dx,string
    add dx,2
    mov ah,09h
    int 21h

    pop dx
    pop ax
    endm

print_string macro string
    push ax
    push dx

    lea dx, string
    mov ah, 09h
    int 21h

    pop dx
    pop ax
    endm

print_newline macro
    push ax
    push dx

    lea dx, newline
    mov ah, 09h
    int 21h

    pop dx
    pop ax
    endm

print_line_sums macro line_sums
    local i_loop
    push cx
    push si
    push ax

    mov cx, 3
    mov si,2
    xor ax,ax
    i_loop:
        print_line_sum_msg al
        print_number line_sums[si]
        print_newline
        inc al
        add si, 2
        loop i_loop

    pop ax
    pop si
    pop cx
    endm

init_data macro
    mov ax, data            ;\
    mov ds, ax              ; > �������� �������� ������
    mov es, ax              ;/
    endm

exit macro
    mov ax,4c00h
    int 21h
    endm

.code
    start:
        init_data
        enter_matrix matrix
        print_matrix matrix
        ;matrix_sum matrix
        ;print_string sum_msg
        ;print_number sum
        ;print_newline
        ;matrix_line_sum matrix 
        ;print_line_sums line_sums 
        exit 


;-----------------------------------------------------------------
; ���������� ����� � ������
;
; ����: dx - ����� ������, � ax - �����, � cx - ����� ��������
;
; �����: � ������� �������� ����� � ��������� ����
convert_to_string proc near
    push bx
    push si
    push di
    

    xor si, si
    mov bx, dx
    mov di, 10 ;��������� ��������
    push cx ;��������� ����� ��������
    push ax ;��������� �����
    cmp ax, 0 ;�������� ����� � ����

    js negative1
    convert_to_string_loop:
        xor dx, dx ;�������� dx, ������ ��� ��� ����� ������ ������� �� �������
        div di ;�������� ����� �� 10. ������� �� ������� ����� � dl
        add dl, '0' ;�������� ������ �����
        mov [bx+si], dl ;������� ����� � ������ 
        inc si ;������� � ���������� �������
        loop convert_to_string_loop
        pop ax ;������������ �����
        pop cx ;������������ ����� ��������
        ;��������� ���������� ����� ��������� � si ������ ������, � � di � ��������� ������
        ;����� � ������ �������� � �������� �������, ������� ��� ���������� �����������
        mov si,0 ;����� ������� ������� ����� � ������
        mov di,cx ;(����� ���������� ������� ����� � ������)+1
        dec di 
        cmp ax, 0
        js negative2
    negative2_back:
        lea bx, number_string
        call revert_number
        
        pop di
        pop si
        pop bx
        ret

    negative1:
        mov [bx+si], '-';������� ����� � �������
        neg ax ;������� ����� �������������
        inc si ;������� �� ��������� ������
        jmp convert_to_string_loop
        
    negative2:;���� ����� �������������, �� ��� ��� ����� ��������� � ������ �� ���� ������ �����
        inc si 
        inc di
        jmp negative2_back
convert_to_string endp

;-----------------------------------------------------------------
; ������ ����� ;
; ����: bx - ����� ������, si - ����� ������� ������� �����, di - ����� ���������� ������� �����
; 
; �����: bx - ������������� ������
revert_number proc near
    push ax
    push dx
    push cx

    cmp si,di;���� ������ ����, �� �������
    je revert_number_quit
   
    mov ax, di
    sub ax, si
    mov cx, 2
    div cl ;����� ���������� � a� = (di-si)/2
    mov cl, al
    add cl, ah ;��������� ������� �� ������� 
    revert_number_loop:
        mov al, [bx+si] 
        mov dl, [bx+di]

        mov [bx+si], dl
        mov [bx+di], al
        inc si
        dec di

        loop revert_number_loop
    revert_number_quit:

        pop cx
        pop dx
        pop ax
        ret
revert_number endp

;-----------------------------------------------------------------
; ���������� ������ � �����
;
; ����: dx - ����� ������
;
; �����: ax - �����, � dx - 0, ���� �� � �������, 1, ���� ������(������ �������������, ������ ������������)
convert_to_number proc near
    push cx
    push si
    push bx

    xor cx, cx
    str_len string, cl
    mov bx, dx
    xor ax, ax
    mov si, 2
    convert_to_number_loop:
        xor dx, dx
        mov dl, [bx+si]
        cmp dl, '-'
        jz to_num_negative1
        
        sub dl, '0'
        add ax, dx
        cmp cx, 1
        jnz move_to_the_left
        inc si
        loop convert_to_number_loop
         
        mov dl, [bx+2]
        cmp dl, '-'
        jz to_num_negative2
        to_num_negative2_back:
        xor dx,dx
        pop bx
        pop si
        pop cx
        ret
        
    move_to_the_left:
        mov dx, 10
        imul dx
        jo value_error ;��������� ������������, �.�. ���� ������ 32767, ���� ������ -32768 
        inc si
        loop convert_to_number_loop
    to_num_negative1:
        inc si 
        loop convert_to_number_loop
        
    to_num_negative2:
        neg ax
        jmp to_num_negative2_back

    value_error:
        print_string value_error_msg
        print_newline
        lea dx, string
        call clear_string
        mov dx, 1
        pop bx
        pop si
        pop cx
        ret
convert_to_number endp

;-----------------------------------------------------------------
; ������� ������
;
; ����: ����� ������ � dx
;
; �����: ������ ������
clear_string proc near
    push bx
    push si
    push cx
    
    lea bx, string
    mov [bx+1], 0
    mov cx, 8
    mov si,2
    clear_string_loop:
        mov [bx+si], '$'
        inc si
        loop clear_string_loop
        
        pop cx
        pop si
        pop bx
        ret
clear_string endp

;-----------------------------------------------------------------
; ���� ������ � ���������� (������, �������� �� ������)
;
; �����: � dx - ����� ������ 
input_string proc near
    push cx
    push si
    push bx

    input_string_loop:
        print_input_msg i,j
        enter_string string
        print_newline
        
        xor cx,cx
        str_len string, cl
        
        lea bx, string
        mov si, 2
        mov dl,[bx+si]
        cmp dl, '-'
        jz  is_negative

    is_negative_back:
        cmp cl, 0
        jz size_error
        cmp cl, max_strlen
        ja size_error

    char_check_loop:
        mov dl, [bx+si]
        cmp dl, '0'
        jb not_a_number_error
        cmp dl, '9'
        ja not_a_number_error
        inc si
        loop char_check_loop
        
        lea dx, string
        pop bx
        pop si
        pop cx
        ret

    size_error:
        print_string size_error_msg
        print_newline
        call clear_string
        jmp input_string_loop

    not_a_number_error:
        print_string not_a_number_error_msg
        print_newline
        call clear_string
        jmp input_string_loop

    is_negative:
        dec cl
        inc si
        jmp is_negative_back
input_string endp



end start


