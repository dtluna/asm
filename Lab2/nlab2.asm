.model small
.stack 100h
.data
    messageError0 db "Error. Max number in array = 0", '$'
    messageError1 db "Error on input of number.", '$'
    messageError2 db "Error. Some number is below -128.", '$'
    messageError3 db "Error. You have entered less than 6 numbers.", '$'
    messageError4 db "Error. You have entered more than 6 numbers.", '$'
    messageError5 db "Error. Some number is greater than 999.", '$'
    messageError6 db "Error. Some number is greater than 127.", '$'
    messageError7 db "Error. Not a digit was found.", '$'
    message db "Matrix 5x6.", '$'
    enterNumsMsg db "Enter 30 numbers between [-128; 127]. ", '$'
    enterRowMsg db "Enter row(6 numbers) : ", '$'
    sumOfArrayMsg db "Sum of array: ", '$'
    sumOfColsMsg db "Sum of columns : ", '$'
    maxSumRowsMsg db "Numbers of rows with max sum: sum = ", '$'
    inRowsMsg db " in rows ", '$'
    buf db 5, 5 dup ('$')
    NewLine db 10,13, '$'
    string db 50, 50 dup ('$')
    array db 30 dup (?)
    ten db 10
    max dw ?
    sum dw ?
    number dw ?

.code
start:   
    mov ax, @data
    mov ds, ax
    
    lea dx, message
    mov ah, 9
    int 21h 
    
    lea dx, NewLine
    mov ah, 9
    int 21h
    
    lea dx, enterNumsMsg
    mov ah, 9
    int 21h
    
    lea dx, NewLine
    mov ah, 9
    int 21h
    
    xor cx, cx
    xor di, di 
    xor si, si
    xor bx, bx
    
input:
    inc cl  
    
    lea dx, enterRowMsg
    mov ah, 9
    int 21h
    
    lea dx, string
    mov ah, 0Ah
    int 21h
    
    lea dx, NewLine
    mov ah, 9
    int 21h
    
    call normalizeString
    call addStoEndOfString
    add bx, 6
    call convertStringToNumbers
    
    cmp si, bx
        jl errorLessThan6Numbers
    cmp cl, 5
        jne input
    
    call sumOfArray           ;// 191
                                ;
    lea dx, sumOfArrayMsg     ;
    mov ah, 9                   ;
    int 21h                     ;
                                ;
    mov ax, sum               ; sumOfMatrix
                                ;
    call numberAXtoString       ; 215
                                ;
    lea dx, buf                 ;
    mov ah, 9                   ;
    int 21h                     ;//
      
    lea dx, NewLine
    mov ah, 9
    int 21h
    
    lea dx, sumOfColsMsg     ;//
    mov ah, 9                   ;
    int 21h                     ; sumOfColumns
                                ;
    call sumOfColumns         ;//
    
    lea dx, NewLine
    mov ah, 9
    int 21h
    
    lea dx, maxSumRowsMsg     ;//
    mov ah, 9                   ;
    int 21h                     ; maxSumOfRows
                                ;
    call findMaxSumInRow        ;//
    
    lea dx, NewLine
    mov ah, 9
    int 21h
    
    call normalizeArray
    
    call outputOfMatrix 
    
    jmp endOfProgram

errorDivideZero:
    xor ax, ax
    lea dx, messageError0
    mov ah, 9
    int 21h
    jmp endOfProgram

errorMinusFind:
    xor ax, ax
    lea dx, messageError1
    mov ah, 9
    int 21h
    jmp endOfProgram
    ; '-' was entered

errorInputNegNumber:
    ;entered number less than -128
    xor ax, ax
    lea dx, messageError2
    mov ah, 9
    int 21h
    jmp endOfProgram 

errorLessThan6Numbers:
    ;entered less than 6 numbers
    xor ax, ax
    lea dx, messageError3
    mov ah, 9
    int 21h
    jmp endOfProgram

errorMoreThan6Numbers:
    ;entered more than 6 numbers
    xor ax, ax
    lea dx, messageError4
    mov ah, 9
    int 21h
    jmp endOfProgram

errorLongNumber:
    ;entered number > 999
    xor ax, ax
    lea dx, messageError5
    mov ah, 9
    int 21h
    jmp endOfProgram

errorBigNumber:
    ;entered number > 127
    xor ax, ax
    lea dx, messageError6
    mov ah, 9
    int 21h
    jmp endOfProgram
    
errorNotOnlyDigitals:
    ;no-digit character in string
    xor ax, ax
    lea dx, messageError7
    mov ah, 9
    int 21h
  
endOfProgram:    
    mov ax, 4C00h
    int 21h 
    
    
convertStringToNumbers:
    push di
    push cx
    push bx
    push ax
    xor di, di
    
    inc di
    xor cx, cx
    xor ax, ax
findNumber:
    inc cl  ; lengthOfNumber
    inc di
    cmp string[di], '$'
        je endOfConvert
    cmp string[di], 13
        je makeNumber
    cmp string[di], 32
        jne findNumber
        
makeNumber:
    mov number, 0
    xor ch, ch
    push di     ; for search continue
    sub di, cx
    dec cl

    cmp cl, 4
        jg errorLongNumber
    cmp string[di+1], 45      ; if number is negative
        jne loop1
    inc di      ; for minus
    dec cl      ; for minus
    cmp cl, 0
        je errorMinusFind    
    mov ch, 1               ; 1 in ch = negative number
loop1:
    cmp cl, 4
        jg errorLongNumber
    mov number, ax
    cmp cl, 0
        je addNumberToArray        
    dec cl
    inc di
    mul ten
    push bx
    mov bl, string[di]
    sub bl, 48
    cmp bl, 9
        jg errorNotOnlyDigitals
    cmp bl, 0
        jl errorNotOnlyDigitals    
    add ax, bx
    pop bx
    
    jmp loop1
addNumberToArray:
    cmp ch, 0       ; 0 = positive number
        je posNumber
    cmp ax, 128                 ; check of neg number
        jg errorInputNegNumber
    neg ax
posNumber:
    cmp ax, 127                 ; check of pos number
        jg errorBigNumber
    mov array[si], al
    xor ax, ax
    inc si
    cmp si, bx
        jg errorMoreThan6Numbers
    pop di      ; for search continue
    jmp findNumber    
endOfConvert:
    
    pop ax
    pop bx
    pop cx
    pop di
    
    ret     

sumOfArray:
    push di
    push ax
    
    xor di, di
    xor ax, ax
loopSum:
    mov al, array[di]
    inc di
    cmp al, 0
        jl belowZero
    add sum, ax
    cmp di, 30
        jne loopSum
        je endSumOfArray
belowZero:
    neg al
    sub sum, ax
    cmp di, 30
        jne loopSum
endSumOfArray:        
    pop ax
    pop di
    ret
    
numberAXtoString:
    push di
    push si
    push ax
    xor di, di
    xor si, si
    cmp ax, 0
        jge loopPositive
    inc di
    neg ax
    mov buf[si], 45
    inc si          ; first symbol '-'
loopPositive:
    add di, 4
    mov bx, 10
loopN:
    dec di
    xor dx, dx    
    div bx
    add dl, 48
    mov buf[di], dl
    cmp al, 0
        jne loopN
loopShift:
    cmp si, di
        je endloopN
    mov dl, buf[di]         
    mov buf[si], dl
    inc di
    inc si
    cmp di, 5
        jne loopShift
fillbufS:        
    mov buf[si], '$'
    inc si
    cmp si, 5
        jle fillbufS     
endloopN:
    pop ax
    pop si
    pop di
    ret

sumOfColumns:
    push di
    push ax
    push si
    
    xor ax, ax
    xor di, di
    xor si, si
    
    mov sum, 0
    
startSumOfColumns:
    
    mov al, array[di]
    cmp al, 0
        jl belowZero1
    add sum, ax
    add di, 6
    cmp di, 30
        jl startSumOfColumns
        jge continueSumOfColumns
belowZero1:
    neg al
    sub sum, ax
    add di, 6
    cmp di, 30
        jl startSumOfColumns
        jge continueSumOfColumns    
    
continueSumOfColumns:
    mov ax, sum
    
    call  numberAXtoString
    
    lea dx, buf                 
    mov ah, 9                   
    int 21h
    
    xor ax, ax
    mov dl, 32  
    mov ah, 6
    int 21h
    
    xor ax, ax
    mov sum, 0
    inc si
    mov di, si
    cmp si, 6
        jne startSumOfColumns
    
    pop si
    pop ax
    pop di
    ret

findMaxSumInRow:
    push di
    push ax
    push bx
    push dx
    push si
    
    xor di, di
    xor dx, dx
    xor ax, ax
    xor si, si
     
    mov sum, 0
    mov max, -768
    add si, 6
startFindSum:
    cmp di, 30
        je compareSum
    mov dl, array[di]
    cmp dl, 0
        jl belowZero2
    add sum, dx
    inc di
    cmp di, si
        jl startFindSum
        jge continueFindSum
belowZero2:
    neg dl
    sub sum, dx
    inc di
    cmp di, si
        jl startFindSum
        jge continueFindSum
continueFindSum:                
    add si, 6
    mov ax, sum
    mov sum, 0
    cmp ax, max
        jle startFindSum 
        
    mov max, ax
    cmp si, 30
        jle startFindSum
        
compareSum:
    mov ax, max
    call numberAXtoString
    
    lea dx, buf                 
    mov ah, 9                   
    int 21h
    
    lea dx, inRowsMsg                 
    mov ah, 9                   
    int 21h
    
    xor dh, dh
            
    xor di, di
    xor si, si
    add si, 6
    mov sum, 0
    
startCompareSum:
    cmp di, 30
        je endCompareSum
    mov dl, array[di]
    cmp dl, 0
        jl belowZero3
    add sum, dx
    inc di
    cmp di, si
        jl startCompareSum 
        jge continueCompareSum   
belowZero3:
    neg dl
    sub sum, dx
    inc di
    cmp di, si
        jl startCompareSum
        jge continueCompareSum
        
continueCompareSum:            
    add si, 6
    mov ax, sum
    mov sum, 0
    cmp ax, max
        jne startCompareSum
        
    ;print string index
    mov ax, si
    mov bl, 6
    div bl
    
    xor dx, dx
    mov dl, al
    add dl, 47      ;index+1 on al
    mov ah, 6
    int 21h     ; output of index
    
    xor ax, ax
    mov dl, 32  
    mov ah, 6
    int 21h     ; output of space
    
    cmp si, 30
        jle startCompareSum
endCompareSum:            
    pop si
    pop dx
    pop bx
    pop ax
    pop di
    ret 

outputOfMatrix:
    push di
    push si
    push ax
    push dx
    
    xor di, di
    xor ax, ax
    xor dx, dx
    xor si, si
nextRow:
    cmp si, 30
        je endOfoutput     
    add si, 6
    
    lea dx, NewLine
    mov ah, 9
    int 21h
nextNumber:
    xor ah, ah    
    mov al, array[di]
    cmp al, 0
        jg continueNextNumber
    neg al
    neg ax     
continueNextNumber:        
    inc di
    call numberAXtoString
    xor ax, ax
    
    lea dx, buf                 
    mov ah, 9                   
    int 21h
    
    mov dl, 32  
    mov ah, 6
    int 21h
    
    cmp di, si
       je nextRow
       jl nextNumber
       
endOfoutput:        
    pop dx
    pop ax
    pop si
    pop di
    ret
    
    
findMaxInArray:
    push di
    push ax
    push cx
    push dx 
    
    xor di, di
    xor ax, ax
    xor cx, cx
    xor dx, dx

    
    mov max, -128
    
nextNumberFind:    
    xor ah, ah    
    mov al, array[di]
    cmp al, 0
        jge posNumberFind
    neg al
    neg ax
posNumberFind:
    inc di
    cmp di, 30
        je endFindMax
    cmp ax, max
        jle nextNumberFind 
    mov max, ax
    ;neg max
    jmp nextNumberFind   
endFindMax:    
    
    pop dx
    pop cx
    pop ax
    pop di
    ret
    
    
normalizeArray:
    push di
    push ax
    push bx
    push dx
    
    xor di, di
    xor ax, ax
    xor dx, dx
    
    call findMaxInArray
    mov bx, max
    cmp bx, 0
        jg startNormalize
        je errorDivideZero
    neg bx
startNormalize:
    xor ah, ah
    xor bh, bh
    mov al, array[di]
    cmp al, 0
        jge posNumber3
    neg al
    mov bh, 1               ; 1 - negative number    
posNumber3:        
    div bl
    cmp bh, 1
        je setNegNumberToArray

    mov array[di], al
         
    mov array[di], al
    inc di
    cmp di, 30
        jl startNormalize;
setNegNumberToArray:
    cmp max, 0
        jl continueSetNegNumber        
    neg al
continueSetNegNumber:

    mov array[di], al 
    inc di
    cmp di, 30
        jl startNormalize;
                       
    pop dx
    pop bx
    pop ax
    pop di
    ret
    
    
normalizeString:
    push di
    push si
    push ax
Start1:
    xor di, di
    inc di
    xor si, si
    inc si
    xor ax, ax

    cmp string[2], 32
        je shift
        jne findFirstSpace 
end1:
    pop ax
    pop si
    pop di        
    ret
shift:
    mov si, di
shift1:
    inc si
    mov al, string[si]
    mov string[si-1], al
    cmp string[si], '$'
        jne shift1
    jmp Start1
findFirstSpace:
    inc di
    cmp string[di], '$'
        je end1
    cmp string[di], 32
        je findSecondSpace
        jne findFirstSpace  
findSecondSpace:
    inc di
    cmp string[di], 32
        je shift
        jne findFirstSpace


addStoEndOfString:
    push di
    xor di, di
    inc di
loopS:
    inc di
    cmp string[di], 13
        jne loopS
    cmp string[di-1], 32
        jne endLoopS
    mov string[di-1], 13
    mov string[di], '$'     
endLoopS:
    mov string[di+1], '$'   
    pop di
    ret    
end start    
