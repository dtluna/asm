;������������ ������� ����� ����� (������ ������� 5�6 ���������).
;�������� ���������:
;-- ���������� ����� ��������� �������;
;-- ����� ����� (��������, ������������) ��������� ����� (��������);
;-- ����� ������ ����� (��������) � ������������ ������ (���������,
;   �������������) ���������;
;-- ������������� ������� �� ������������� �������� �� ���������;
;-- ������������� ������ (�������) �� ����������� (��������) ��������
;   ������������ ��������� (�������� ������������� � ������������ ��������);
;-- ���������� ������� � �������� �������� �������� 3*3 (5*5).

.model small

.stack
    dw 400h dup(0)

max_lines  = 3
max_rows   = 3
max_strlen  = 3
i_pos_input = 8
j_pos_input = 11
i_pos_line_sum = 5
matrix_size = max_rows*max_lines

.data 
    input_msg              db "Enter m[i][j]:$";8,11
    matrix_msg             db "Matrix:$"
    line_sum_msg           db "Line[i] sum:$";5
    sum_msg                db "Matrix sum:$"
    size_error_msg         db "Invalid number length! Max: 3 digits, min: 1 digit.$"
    not_a_number_error_msg db "The string is not a number!$"
    value_error_msg        db "Invalid value: number is more than 127 or less than -128!$"
    newline                db 10,13,'$'
    space                  db " $"
    i                      db 0
    j                      db 0

    o_number_string        db 7, 7 dup('$')
    i_number_string        db 9, 9 dup('$');������������ ���������� �������� ��� ����� � 6

    matrix                 db max_lines*max_rows, max_lines*max_rows dup(0)

    sum                    dw 0
    line_sums              dw max_lines, max_lines dup(0)

;���� ���� ���� ������� ������� ���� ���� ����

init_data macro
    mov ax, data            ;\
    mov ds, ax              ; > �������� �������� ������
    mov es, ax              ;/
    endm

exit macro
    mov ax,4c00h
    int 21h
    endm

print_string macro string
    push ax
    push dx

    lea dx, string
    mov ah, 09h
    int 21h

    pop dx
    pop ax
    endm

print_user_string macro string
    push ax
    push dx

    lea dx,string
    add dx,2
    mov ah,09h
    int 21h

    pop dx
    pop ax
    endm

print_matrix_msg macro
    push ax
    push dx

    lea dx,matrix_msg
    mov ah,09h
    int 21h

    pop dx
    pop ax
    endm

clear_o_string macro string
    local clear_o_string_loop
    push di
    push cx
    
    mov di,1
    mov cx,6
    
    clear_o_string_loop:
         mov string[di],'$'
         inc di
         loop clear_o_string_loop
    
    pop cx
    pop di
    endm

clear_string macro string
    local clear_string_loop
    push di
    push cx

    mov di,1
    mov string[di],0 ;������������� ����� �������� ��������� � 0
    go_to_first_char di ;���������� �� ������ ������
    mov cx, 7 ;������� �������� ����� ��������
    clear_string_loop:
        mov string[di],'$'
        inc di
        loop clear_string_loop

    pop cx
    pop di
    endm

print_line_sums_msg macro i
    push ax
    
    mov al, i
    add al,'0'
    mov line_sum_msg[i_pos_line_sum], al
    print_string line_sum_msg 
    
    pop ax
    endm

print_input_msg macro i,j
    push ax

    mov al,i
    mov ah,j
    add al,'0'
    add ah,'0'
    mov input_msg[i_pos_input], al
    mov input_msg[j_pos_input], ah
    print_string input_msg

    pop ax
    endm

enter_string macro string
    push ax
    push dx
    
    lea dx, string
    mov ah, 0Ah
    int 21h
    
    pop dx
    pop ax
    endm

str_len macro string, reg ;� reg ����� ����� ������
    push bx
    
    lea bx, string
    inc bx
    mov reg, [bx]
    
    pop bx
    endm

print_newline macro
    push ax
    push dx

    lea dx, newline
    mov ah, 09h
    int 21h

    pop dx
    pop ax
    endm

print_space macro
    push ax
    push dx

    lea dx, space
    mov ah, 09h
    int 21h

    pop dx
    pop ax
    endm

go_to_first_char macro reg
    mov reg, 2
    endm

;���� ���� ���� ������� ����� ���� ���� ����

input_string macro string
    local input_string_loop, is_negative_back, is_negative, char_check_loop, size_error, not_a_number_error, quit

    push cx
    push si
    push ax

    input_string_loop:
        print_input_msg i,j
        enter_string string
        print_newline

        xor cx,cx
        str_len string, cl
        go_to_first_char si

        mov al, string[si] 
        cmp al, '-'
        jz  is_negative

        is_negative_back:
            cmp cl, 0
            jz size_error
            cmp cl, max_strlen
            ja size_error

        char_check_loop:
            mov al, string[si] 
            cmp al, '0'
            jb not_a_number_error
            cmp al, '9'
            ja not_a_number_error
            inc si
            loop char_check_loop
        jmp quit
    

    is_negative:
        dec cl
        inc si
        jmp is_negative_back

    size_error:
        print_string size_error_msg
        print_newline
        clear_string string
        jmp input_string_loop

    not_a_number_error:
        print_string not_a_number_error_msg
        print_newline
        clear_string string
        jmp input_string_loop
    
    quit:
        pop cx
	    pop si
	    pop ax
    
    endm

enter_byte macro dest
    local enter_loop
    push dx
    push ax

    enter_loop:
        input_string i_number_string
        convert_to_byte i_number_string, dx
        cmp dx, 1
        je enter_loop
    
    mov dest,al
    pop ax
    pop dx
    endm

enter_word macro dest
    local enter_loop
    push dx
    push ax

    enter_loop:
        input_string i_number_string
        convert_to_word i_number_string, dx
        cmp dx, 1
        je enter_loop
    
    mov dest,al
    pop ax
    pop dx
    endm

enter_matrix macro matrix 
    local i_loop, j_loop
    push bx
    push si
    push cx

    mov cx, max_lines
    mov si,1
    xor bx,bx
    i_loop:
        push cx
        mov cx, max_rows
        xor bh,bh
        j_loop:
            mov i, bl
            mov j, bh
            push cx
            enter_byte matrix[si]
            pop cx
            inc bh
            inc si
            loop j_loop
        inc bl
        pop cx
        loop i_loop
    
    pop cx
    pop si
    pop bx
    endm

;���� ���� ���� ������� ������ ���� ���� ����

print_byte_number macro number
    push ax

    mov al, number
    convert_byte_to_string o_number_string, al
    print_string o_number_string
    clear_o_string o_number_string

    pop ax
    endm

print_word_number macro number
    push ax

    mov ax, number
    convert_word_to_string o_number_string, ax
    print_string o_number_string
    clear_o_string o_number_string
    
    pop ax
    endm

print_matrix macro matrix
    local i_loop, j_loop
    push si
    push cx

    mov cx, max_lines
    mov si,2
    print_matrix_msg
    print_newline
    i_loop:
        push cx
        mov cx, max_rows
        j_loop:
            push cx
            print_byte_number matrix[si]
            pop cx
            print_space
            add si,2
            loop j_loop
        print_newline
        pop cx
        loop i_loop
    
    pop cx
    pop si
    endm

print_line_sums macro line_sums
    local i_loop
    
    push si
    push cx
    push bx
    
    mov cx, max_lines
    mov si,2
    xor bx,bx
    i_loop:
       mov i, bl
       print_line_sums_msg i
       print_word_number line_sums[si]
       print_newline
       inc bl
       add si,2
       loop i_loop 
    
    pop bx
    pop cx
    pop si
    endm

;���� ���� ���� ��������������� ������� ���� ���� ����
convert_to_byte macro string, error_reg; ��������� � al; 0, ���� �� � �������, 1, ���� ������(������ �������������, ������ ������������)
    local convert_to_number_loop, move_to_the_left, negative1, negative2, value_error, quit 
    push cx
    push si
    push bx

    xor cx, cx
    str_len string, cl
    go_to_first_char si
    
    xor ax,ax
    convert_to_number_loop:
        xor bx,bx
        mov bl, string[si]
        cmp bl, '-'
        je negative1

        sub bl, '0'
        add al, bl
        cmp cx, 1
        jnz move_to_the_left
        inc si
        loop convert_to_number_loop

    mov bl, string+2
    cmp bl, '-'
    je negative2
    jmp quit

    move_to_the_left:
        mov bl, 10
        imul bl
        jc value_error
        jo value_error ;��������� ������������, �.�. ���� ������ 127, ���� ������ -128 
        inc si
        loop convert_to_number_loop

    negative1:
        inc si 
        loop convert_to_number_loop

    negative2:
        neg al
        jmp quit

    value_error:
        print_string value_error_msg
        print_newline
        clear_string string
        mov error_reg, 1
        jmp quit

    quit:
        pop bx
        pop si
        pop cx

    endm

convert_to_word macro string, error_reg; ��������� � ax; 0, ���� �� � �������, 1, ���� ������(������ �������������, ������ ������������)
    local convert_to_number_loop, move_to_the_left, negative1, negative2, value_error, quit 
    push cx
    push si
    push bx

    xor cx, cx
    str_len string, cl
    go_to_first_char si
    
    xor ax,ax
    convert_to_number_loop:
        xor bx,bx
        mov bl, string[si]
        cmp bl, '-'
        je negative1

        sub bl, '0';�������� � bx �����
        add ax, bx
        cmp cx, 1
        jnz move_to_the_left
        inc si
        loop convert_to_number_loop

    mov bl, string+2
    cmp bl, '-'
    je negative2
    jmp quit

    move_to_the_left:
        mov bl, 10
        imul bl
        jc value_error
        jo value_error ;��������� ������������, �.�. ���� ������ 32767, ���� ������ -32768 
        inc si
        loop convert_to_number_loop

    negative1:
        inc si 
        loop convert_to_number_loop

    negative2:
        neg ax
        jmp quit

    value_error:
        print_string value_error_msg
        print_newline
        clear_string string
        mov error_reg, 1
        jmp quit

    quit:
        pop bx
        pop si
        pop cx

    endm

convert_byte_to_string macro string, number
    local convert_to_string_loop, negative1, negative2, negative2_back, revert, quit
    push cx
    push bx
    push ax
    push si
    push di

    count_byte_digits number, si
    mov cx, si
    xor di,di
    mov al, number ;��� �������
    mov bl, 10 ;���������� ��������
    push cx ;��������� ����� ��������
    push ax ;��������� �����

    cmp al, 0 ;�������� ����� � ����
    js negative1

    convert_to_string_loop:
        xor ah, ah ;�������� ah, ������ ��� ��� ����� ������ ������� �� �������
        div bl ;�������� ����� �� 10. ������� �� ������� ����� � ah
        add ah, '0' ;�������� ������ �����
        mov string[di], ah ;������� ������ � �������
        inc di ;������� �� ��������� ������
        loop convert_to_string_loop

    pop ax ;������������ �����
    pop cx ;������������ ����� ��������
    ;���� ����� � ������� �������� � �������� �������, ������� ��� ���������� �����������
    ;� di ����� ����� ���������� ������� + 1
    ;������ ����� ������� ������� � si
        dec di
        mov si, 0
        cmp al, 0
        js negative2

        negative2_back:
        cmp si, di
        je quit ;���� ����� ������� �� ������ �������, �� � ������ ������ �� ����

        revert_string string, si, di
        jmp quit

        negative1:
            mov string[di], '-';������� ����� � �������
            neg al ;������� ����� �������������
            inc di ;������� �� ��������� ������
            jmp convert_to_string_loop

        negative2:
            inc si
            jmp negative2_back

        quit:
            pop di
            pop si
            pop ax
            pop bx
            pop cx

        endm

convert_word_to_string macro string, number
    local convert_to_string_loop, negative1, negative2, negative2_back, revert, quit
    push cx
    push bx
    push ax
    push si
    push di

    count_word_digits number, si
    mov cx, si
    xor di,di
    mov ax, number ;��� �������
    mov bx, 10 ;���������� ��������
    push cx ;��������� ����� ��������
    push ax ;��������� �����

    cmp ax, 0 ;�������� ����� � ����
    js negative1

    convert_to_string_loop:
        xor dx, dx ;�������� dx, ������ ��� ��� ����� ������ ������� �� �������
        div bx ;�������� ����� �� 10. ������� �� ������� ����� � dl
        add dl, '0' ;�������� ������ �����
        mov string[di], dl ;������� ������ � �������
        inc di ;������� �� ��������� ������
        loop convert_to_string_loop

    pop ax ;������������ �����
    pop cx ;������������ ����� ��������
    ;���� ����� � ������� �������� � �������� �������, ������� ��� ���������� �����������
    ;� di ����� ����� ���������� ������� + 1
    ;������ ����� ������� ������� � si
        dec di
        mov si, 0
        cmp ax, 0
        js negative2

        negative2_back:
        cmp si, di
        je quit ;���� ����� ������� �� ������ �������, �� � ������ ������ �� ����

        revert_string string, si, di
        jmp quit

        negative1:
            mov string[di], '-';������� ����� � �������
            neg ax ;������� ����� �������������
            inc di ;������� �� ��������� ������
            jmp convert_to_string_loop

        negative2:
            inc si
            jmp negative2_back

        quit:
            pop di
            pop si
            pop ax
            pop bx
            pop cx

        endm

revert_string macro string, first_char, last_char
    local revert

    push ax
    push cx
    push di
    push si

    mov ax, last_char
    sub ax, first_char
    mov cx, 2
    div cl ;����� ���������� � a� = (di-si)/2
    mov cl, al
    add cl, ah ;��������� ������� �� ������� 

    mov si, first_char
    mov di, last_char

    revert:
        mov al, string[si]
        mov ah, string[di]
        mov string[si], ah
        mov string[di], al
        inc si
        dec di
        loop revert

    
    pop si
    pop di
    pop cx
    pop ax

    endm

count_word_digits macro number, digits ;� digits ����� ����� ��������
    local count_digits_loop, quit, negative, negative_back, more
    push bx
    push ax
    push dx
    push cx
    push di
    
    mov bx, number
    cmp bx, 0
    js negative ;���� ����� <0
    negative_back:
        mov ax,9 ;����� ������� ����� � ����� ������
        mov cx,1 ;����� � ����� ��� ������� ����
        mov di,10 ;��������� ���������
        xor dx,dx ;�������� "������� �����"
    count_digits_loop:
        cmp bx, ax ;���� ���� ����� ������ ������ �������� ����� � ������ ����������� ����   
        ja more
        jna quit

    negative:
        neg bx ;������ �������������
        jmp negative_back

    more:
        inc cx ;����������� ������� ����
        mul di   ; | ����������� ������ �������
        add ax,9 ;/
        jmp count_digits_loop
        
    quit:
        mov digits, cx
        pop di
        pop cx
        pop dx
        pop ax    
        pop bx
    endm

count_byte_digits macro number, digits ;� digits ����� ����� ��������
    local count_digits_loop, quit, negative, negative_back, more 
    push bx
    push ax
    push cx
    
    mov bl, number
    cmp bl, 0
    js negative ;���� ����� <0
    negative_back:
        mov al,9 ;����� ������� ����� � ����� ������
        mov cx,1 ;����� � ����� ��� ������� ����
        mov bh,10 ;��������� ���������
    count_digits_loop:
        cmp bx, ax ;���� ���� ����� ������ ������ �������� ����� � ������ ����������� ����   
        ja more
        jna quit

    negative:
        neg bx ;������ �������������
        jmp negative_back

    more:
        inc cx ;����������� ������� ����
        mul bh   ; | ����������� ������ �������
        add al,9 ;/
        jmp count_digits_loop
        
    quit:
        mov digits, cx
        pop cx
        pop ax    
        pop bx
        endm

;���� ���� ���� ������� ����ר�� ���� ���� ���� ����
matrix_sum macro matrix, sum
    local sum_loop
    push ax
    push si
    
    mov cx,matrix_size
    mov si,1 
    sum_loop:
        mov al, matrix[si]
        cbw
        add sum, ax
        inc si 
        loop sum_loop
    
    
    pop si
    pop ax
    
    endm

matrix_line_sums macro matrix, line_sums
    local i_loop, j_loop
    push ax
    push si
    push di
    
    mov si,1;������ ������� �������
    mov di,2;������ ������� ������� ����
    
    mov cx, max_lines
    
    i_loop:
       push cx
       xor ax,ax
       mov cx,max_rows
       j_loop:
           mov al, matrix[si]
           cbw
           add line_sums[di], ax
           inc si
           loop j_loop
       add di,2
       pop cx
       loop i_loop  
    
    pop di
    pop si
    pop ax
    endm
;���� ���� ���� ������� ��� ���� ���� ����

.code
    start:
        init_data
        ;enter_matrix matrix
        ;print_string sum_msg
        ;matrix_sum matrix, sum
        ;print_word_number sum
        ;print_newline
        ;matrix_line_sums matrix, line_sums
        ;print_line_sums line_sums
        ;print_newline
        enter_byte matrix
        print_byte_number matrix
    	exit	
    end start
