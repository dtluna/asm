.text	# section declaration

	# we must export the entry point to the ELF linker or
	.global _start	# loader. They conventionally recognize _start as their
	# entry point. Use ld -e foo to override the default.

STDOUT = 1

SYS_EXIT = 1
SYS_WRITE = 4

.macro call_kernel
	int		$0x80	# call kernel
.endm

.macro stdout_print message, length
	push %rax
	push %rbx
	push %rcx
	push %rdx

	mov $STDOUT, %ebx	# first argument: file handle (stdout)
	mov \message, %ecx	# second argument: pointer to message to write
	mov \length, %edx	# third argument: message length
	mov $SYS_WRITE, %eax	# system call number (sys_write)
	call_kernel

	pop %rdx
	pop %rcx
	pop %rbx
	pop %rax
.endm

_stdout_print:

	ret

#first argument: pointer to message to write
#second argument: message length
/*stdout_print:
	pushl	%ebp
	movl    %esp,%ebp

	movl	$STDOUT, %ebx	# first argument: file handle (stdout)
	movl	1ST_ARG, %ecx 	# second argument: pointer to message to write
	movl	(%ebp+6), %edx 	# third argument: message length
	movl	$SYS_WRITE, %eax	# system call number (sys_write)
	call_kernel
	ret 
*/

#increases number by 1
#1st - number
#result in eax
a:
	push %rbp

	mov %rsp,%rbp

	movl 16(%rbp), %eax
	inc %eax

	pop %rbp
	ret $16

.macro close_program exit_code
	push \exit_code
	call _close_program
.endm

_close_program:
	push %rbp
	mov %rsp,%rbp
	mov 4(%rbp), %ebx	# first argument: exit code
	mov $SYS_EXIT, %eax	# system call number (sys_exit)
	call_kernel
	pop %rbp
	ret



_start:
	push len
	call a
	movl %eax, len
	# write our string to stdout
	stdout_print $msg, len
	# and exit

	close_program $0

.data	# section declaration

msg:
	.ascii	"Hello, world!\n"	# our dear string
len:
	.int	. - msg -1	# length of our dear string
