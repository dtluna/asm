.text
.global     _start                              #must be declared for linker (ld)

_start:                                          #tell linker entry point

    movl    $len, %edx                          #message length
    movl    $msg, %ecx                          #message to write
    movl    $1,%ebx                             #file descriptor (stdout)
    movl    $4,%eax                             #system call number (sys_write)
    int     $0x80                               #call kernel

    movl    $1,%eax                             #system call number (sys_exit)
    int     $0x80                               #call kernel

.data

#MISMATCH: "msg     db  'Hello, world!',0xa                 "
#MISMATCH: "len     equ $ - msg                             "
msg:
	.ascii    "Hello, world!\n"   # our dear string
	len = . - msg                 # length of our dear string
