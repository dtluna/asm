.model small
.stack 100h
.data
    message1 db "Input string which you want to replace: ", '$'
    message2 db "Input string to which you want to replace: ", '$'
    filePathInput db 'MyFiles\input.txt',0
    qwer db '123.txt',0
    PI db 'MyFiles\123.txt',0
    PO db 'C:\emu8086\MyBuild\MyFiles\output.txt',0
    filePathOutput db 'MyFiles\output.txt',0
    idFileInput dw ?
    idFileOutput dw ?
    NewLine db 10,13, '$'
    searchString db 50, 50 dup ('$')
    replaceString db 50, 50 dup ('$')
    string db 102, 102 dup ('$')
.code
start:   
    mov ax, @data
    mov ds, ax
    
    lea dx, message1
    mov ah, 9
    int 21h
    
    lea dx, searchString
    mov ah, 0Ah
    int 21h
    
    lea dx, NewLine
    mov ah, 9
    int 21h
    
    lea dx, message2
    mov ah, 9
    int 21h
    
    lea dx, replaceString
    mov ah, 0Ah
    int 21h 
    
    lea dx, NewLine
    mov ah, 9
    int 21h
       
    xor ax, ax 
    mov ah, 3Dh    ; opening of file
    mov al, 00000010b    ; read only
   
    lea dx, filePathInput
    int 21h
   
    mov idFileInput, ax       ; id of file
   
    lahf                     ;
    and ah, 1                ;
    cmp ah, 0                ;  check
        je fileInputOpened   ;
    call PrintErrorFileOpen  ;

fileInputOpened:
    
    xor ax, ax
    mov ah, 3Ch    ; opening of file
    mov al, 01b    ; write only
    lea dx, filePathOutput
    int 21h 
    
    mov idFileOutput, ax
    
    lahf                     ;
    and ah, 1                ;
    cmp ah, 0                ;  check
        je startReadingFile  ;
    call PrintErrorFileOpen  ; 
   
startReadingFile:
    mov cx, 100
    mov bx, idFileInput
    lea dx, string   ; buffer to read
    mov ah, 3Fh      ; reading from file
    int 21h
    
    mov di, ax
    mov string[di], '$' 
    
    push ax                 ; number of bytes
    lea dx, string
    mov ah, 9
    int 21h
    
    pop ax                  ; number of bytes 
    
    call writeToNewFile  
                     
    cmp ax, cx
        je startReadingFile   
    
endOfProgramm:
    xor dx, dx 
    xor ax, ax
    xor cx, cx
    
    mov ah, 3Eh
    mov bx, idFileInput
    int 21h
    
    ;xor ax, ax
    mov ah, 3Eh
    mov bx, idFileOutput
    int 21h
    
    ;xor ax, ax
    mov ah, 41h
    mov dx, offset PI           ;
    ;lea dx, PI ; delete file
    ;lea dx, filePathInput
    int 21h               ;
    
    ;mov ah, 56h
    ;mov ax, SEG PI
    ;mov es,ax
    ;lea dx, PO
    ;mov di, offset PI
    ;int 21h
    mov ah, 0Dh
    int 21h
        
    mov ax, 4C00h
    int 21h   
   
PrintErrorFileOpen:
    cmp ah, 2        ; file not found
    cmp ah, 3        ; path not found
    cmp ah, 4        ; too many opened files
    cmp ah, 5        ; access denied
    cmp ah, 12       ; incorrect mode of access
    
    jmp endOfProgramm
    
writeToNewFile:
    push ax   ; number of written bytes
    push bx
    push cx
    push di
    push si
    
            ;function for choose what to write 
    
    xor cx, cx
    xor di, di
    dec di
    xor si, si
    mov si, 2
    
startFindFun:
    inc di
    mov al, string[di]
    cmp string[di], '$'
        je endOfWrite
    cmp al, searchString[si]
        jne writeBytes
    inc cl
    inc si
    cmp cl, searchString[1]
        je writeReplaceString
        jl startFindFun
           
writeBytes:
    mov si, 2
    push di
    sub di, cx        
    inc cl    

    xor ax, ax
    mov ah, 40h
    mov bx, idFileOutput
    
    lea dx, string[di]
    int 21h
    pop di
    xor cl, cl
    jmp startFindFun
        
writeReplaceString:
    xor cx, cx
    mov cl, replaceString[1]
    xor ax, ax
    mov ah, 40h
    mov bx, idFileOutput      
    lea dx, replaceString[2]    
    int 21h
    xor cx, cx
    mov si, 2    
    jmp startFindFun        
    
endOfWrite:    
    pop si
    pop di
    pop cx
    pop bx
    pop ax 
    
    ret    
end start