;������� 1 � ��������� ���� �� ����� ������ � ������
;����� 1 = 47, ���. ���. 1 = 3
;����� 2 = 42, ���. ���. 2 = 2
;���������� = 23

;������� 2 � ������� ��� �������� ������ ��������� � ������� ������

;������� 3 � ����� ��������� � ������, ��������� �������.
             
FUNC_CLOSE_PROGRAM = 4Ch
FUNC_OUT_STRING    = 9h 
FUNC_OUT_CHAR      = 2h
FUNC_IN_CHAR       = 0h
FUNC_IN_STR        = 0Ah  

             
.model small

.stack 100h

.data   
    String1             DB "Fluffy pony jumped over the big lazy fat rat...", 0h 
    String2             DB "Big fat rat don't give a single fuck.     "    
    String1_Length      = String2 - String1 - 1
    Msg_InputCharacter  DB "Input character to search for: $" 
    Msg_CRLF            DB 0Dh, 0Ah, '$'   
    Msg_OverlapsCount   DB "Count of overlaps: $" 
    Msg_InputSubstring  DB "Input substring: $" 
    Msg_SubstringFound  DB "Substring found.$"
    Msg_SubstringNFound DB "Substring not found.$"
    Buf_ForValue        DB 33, DUP(0)  
    Buf_ForString       DB 50, DUP(0) 
    BUF_STRING          =  Buf_ForString + 2
    
   
init_data macro
    mov ax, data            ;\
    mov ds, ax              ; > �������� �������� ������
    mov es, ax              ;/
    endm
        
close_program macro
    mov ah, FUNC_CLOSE_PROGRAM
    int 21h
endm  
  
.code
	start:
		init_data
		;CALL FirstTask    
		;CALL SecondTask
		CALL  ThirdTask
		close_program
		
OutLine: 
    ;INPUT: DX - ofsset to string.
    PUSH    AX
    MOV     AH,FUNC_OUT_STRING 
    INT     21h
    POP     AX
    RET 
                 
InChar:   
    ;INPUT:   None
    ;OUTPUT:  AL - Keycode
    MOV     AH,FUNC_IN_CHAR
    INT     16h
    MOV     DL,AL
    MOV     AH,FUNC_OUT_CHAR
    INT     21h
    RET  
    
InStr:         
    PUSH    AX
    PUSH    BX
    MOV     AH,FUNC_IN_STR
    INT     21h
    MOV     BX,DX                                    
    XOR     AX,AX
    MOV     AL,BYTE [BX + 1]    ;AX = length of input
    ADD     BX,AX               ;BX points to end of string - buffer head.
    ADD     BX,2                ;Skip buffer head
    MOV     [BX],0            ;Insert zero-terminator instead of carrige-return terminator.
    POP     BX
    POP     AX
    RET


FirstTask:
    LEA     DI,String1 + 3
    LEA     SI,String2 + 2
    MOV     CX,23
    REP     MOVSW
    RET
    
SecondTask:
    LEA    DX,Msg_InputCharacter
    CALL   OutLine   
    CALL   InChar    ;Result - AL = KeyCode.
    LEA    DX,Msg_CRLF
    CALL   OutLine    
    LEA    DX,Msg_OverlapsCount
    CALL   OutLine
    MOV    CX,String1_Length
    LEA    DI,String1 
    XOR    DX,DX
    CLD 
  ContinueScan:  
    REPNE  SCASB 
    JCXZ   Exit
    INC    DX   
    JMP    ContinueScan
  Exit:    
    ;Now DX - count of symbols
    MOV    BX,10
    LEA    DI,Buf_ForValue
    MOV    AX,DX
    XOR    DX,DX
    PUSHA
    CALL   lpSZ_ConvertTo_
    POPA
    LEA    DX,Buf_ForValue
    CALL   OutLine
    LEA    DX,Msg_CRLF
    CALL   OutLine
    RET  
    
ThirdTask:
    LEA    DX,Msg_InputSubstring
    CALL   OutLine
    LEA    DX,Buf_ForString 
    CALL   InStr 
    LEA    DX,Msg_CRLF
    CALL   OutLine 
              
    LEA    DI,String1    
    XOR    AL,AL
    CALL   wStr_Len_                ;AX = length of String1   
    XOR    BX,BX
CompareLoop:                                               
    PUSH   AX   
    PUSH   BX
    LEA    DI,BUF_STRING  
    LEA    SI,String1   
    ADD    SI,BX                    ;Going to next character of source string.
    XOR    AL,AL
    CALL   IslpSZ_Eq_
    TEST   AX,AX
    JNZ    Found           
    POP    BX           
    POP    AX 
    INC    BX                       ;Next character to start comparing from in next iteration
    DEC    AX                       ;Decrease number of compares left
    JNZ    CompareLoop
    JMP    NotFound
Found: 
    LEA    DX,Msg_SubstringFound
    CALL   OutLine         
    POP    BX           
    POP    AX  
    RET     
NotFound:
    LEA    DX,Msg_SubstringNFound
    CALL   OutLine 
    RET      

       
;----------------------------------------------------------------------------
;----------------------extern W wStr_Len(lpSZ alpszStr)----------------------
;Returns length of the ASCIZ string alpszStr.
;IN:        ES:DI - alpszStr, AL - string terminator value
;OUT:       AX - Length of alpszStr
;Modifies:  AX CX
wStr_Len_:
    PUSH    DI
    MOV     CX,65535         ;Searching in full 16bit segment.
    CLD                      ;Increasing DI every scasb
    REPNE   SCASB            ;Searching...
    MOV     AX,65534         ;AX = full segment - zero value
    SUB     AX,CX            ;AX = length of string
    POP     DI
    RET   
;----------------------------------------------------------------------------
;-------------extern Bool IslpSZ_Eq(lpSZ alpszOne, lpSZ alpszTwo)------------
;Retuns true, if both strings are equal, and false otherwise
;IN:        ES:DI - alpvOne, DS:SI - alpszTwo.
;OUT:       AX - equality flag.
;Modifies:  ES DI AX CX SI
IslpSZ_Eq_:
    PUSH    DS
    CALL    wStr_Len_
    MOV     CX,AX            ;Will compare at least for length of first str.
    ;INC     CX               ;...Plus one char for zero-terminator.
    MOV     AX,1             ;Assuming that strings are equal
    REPE    CMPSB            ;Comparing
    JE      Equal            ;Jump if last chars was equal
    XOR     AX,AX            ;Return false
  Equal:
    POP     DS
    RET
;----------------------------------------------------------------------------
;-----------lpSZ_ConvertTo(lpV alpBuffer, SDW asdwValue, B abBase)-----------
;Converts asdwValue to its string representation in awBase count notation...
;...then puts result in alpBuffer and returns it as well.
;IN:        ES:DI - alpBuffer, DX:AX - asdwValue, BX - abBase
;OUT:       ES:DI - alpBuffer, or NULL of error.
;Modifies:  ES DI DX AX BX SI CX
lpSZ_ConvertTo_: 
    CMP     BX,2             ;Is abBase is less than 2?
    JB      Error            ;If so - exit with error.
    CMP     BX,36            ;Same for 36
    JA      Error
    PUSH    DS               ;Save old DS value
    PUSH    DI               ;...and DI, which will be modified late
    TEST    DH,80h           ;asdwValue is negative?
    JZ      PositiveOne      ;If no - deal with it like with positive.
    NEG     DX               ;In other case - make it positive
    NEG     AX
    SBB     DX,0
    MOV     CS:[bIsNegative],1h
                             ;^And set bIsNegative flag to 1.
  PositiveOne:
    ;PUSH    SEG szAlphabet 
    PUSH    CS
    POP     DS               ;DS = segment of szAlphabet
    MOV     SI,OFFSET szAlphabet    ;SI = offset of szAlphabet
  DivLoop:
    MOV     CX,AX            ;Saving low word of divident
    MOV     AX,DX            ;Will divide high word at first
    XOR     DX,DX            ;Clear remainder
    DIV     BX               ;Division
    XCHG    CX,AX            ;CX - high word of result...
                             ;...AX - low word of divident
    DIV     BX               ;Division on low word of divident.
    XCHG    DX,CX            ;DX - high word of result, CX - remainder
    ADD     SI,CX            ;Correct position in alphabet via remainder
    PUSH    AX               ;Save AX
    MOV     AL,BYTE DS:[SI]  ;AL = char from alphabet
    STOSB                    ;Append char to output buffer
    POP     AX               ;Restore low word of divident
    SUB     SI,CX            ;Restore start position of alphabet
    TEST    AX,AX            ;Is value became zero?
    JNZ     DivLoop          ;If no - continue converting
    CMP     CS:[bIsNegative],1
    JNE     DoNotInsertMinus ;Is value was negative? Jump if not.
    MOV     ES:[DI],'-'      ;Insert minus symbol otherwise.
    INC     DI               ;V...and clear IsNegative status.
    MOV     CS:[bIsNegative],0
  DoNotInsertMinus:
    STOSB                    ;Insert zero-terminator

    POP     DI               ;Restore DI - offset to buffer
    PUSH    DI               ;And saving it again
    MOV     SI,DI            ;SI = DI = start of the result buffer
    XOR     AL,AL
    CALL    wStr_Len_        ;Calculate length of result string
    ADD     SI,AX            ;SI = end of result string + 1
    SHR     AX,1             ;AX = AX / 2 (center of string)
    TEST    AX,AX            ;If AX = 0 then...
    JZ      AfterReverse     ;...There is no need to reverse 1 char.
  MoveLoop:
    DEC     SI               ;Moving to previous symbol (tail)
    MOV     BL,ES:[DI]       ;taking one char
    MOV     BH,ES:[SI]       ;And another one
    MOV     ES:[SI],BL       ;Reversing its places
    MOV     ES:[DI],BH
    INC     DI               ;moving to next symbol (head)
    DEC     AX               ;Iteration is gone
    JNZ     MoveLoop         ;If center is not reached - repeat
  AfterReverse:                
    MOV     ES:[DI + 1],'$'
    POP     DI               ;Restore offset of the result
    POP     DS               ;Restore original DS value
    RET
  Error:
    XOR     DI,DI            ;Returning NULL and exit.
    MOV     ES,DI  
    RET    
    bIsNegative DB 0         ;Is asdwValue negative. Default - no.
    szAlphabet  DB '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
;----------------------------------------------------------------------------
