#!/usr/bin/python
from random import randint
from os import stat

words = ('Linux ', 'Unix ', 'Windows ', 'main ', 'procedure ', 'DOS ')
length = len(words)
filename = 'text.txt'
kbyte = 2**10
textfile = open(filename, 'w')

filesize = stat(filename).st_size

while filesize < 65*kbyte:
	i = randint(0,length-1)
	textfile.write(words[i])
	filesize = stat(filename).st_size
