FUNC_CLOSE_PROGRAM = 4Ch
FUNC_OUT_STRING    = 9h 
FUNC_OUT_CHAR      = 2h
FUNC_IN_CHAR       = 0h
FUNC_IN_STR        = 0Ah  

             
.model small

.stack 100h

.data            
    Buf_Value DB 50 DUP(0) 
    Msg_CRLF            DB 0Dh, 0Ah, '$'   
    
    FiveChars DB "UNIX!" 
    Par1      DW 0
    Par2      DW 0 
    
    Str1      DB  "I am string1$"
    Str2      DB  "And I am the second one$"
    Str3      DB  "Wait for me!$"
    
    proc_addr DW 0
    seg       DW 0
   
init_data macro
    mov ax, data            ;\
    mov ds, ax              ; > �������� �������� ������
    mov es, ax              ;/
    endm
        
close_program macro
    mov ah, FUNC_CLOSE_PROGRAM
    int 21h
endm  
  
.code
	start:        
		init_data
		MOV     CX,5
        LEA     BX,FiveChars
        mov     proc_addr, Func161_Regs
        mov     seg, cs 
	PassByRegs:	
        MOV     DL,[BX] 
        CALL    FAR proc_addr
        INC     BX
        LOOP    PassByRegs
    PassByMem:
        MOV     [Par1],5099
        MOV     [Par2],104
        CALL    Func162_Mem   
    PassByStack:
        mov     proc_addr, Func163_Stack
        mov     seg, cs
        PUSH    OFFSET Str1   
        PUSH    OFFSET Str2
        PUSH    OFFSET Str3
        CALL    FAR proc_addr                
		close_program

;------------------------------	
Func161_Regs PROC FAR    ;Input: DL - character code.
    MOV     AH,FUNC_OUT_CHAR
    INT     21h
    RETF
ENDP  Func161_Regs   	
	       
Func162_Mem PROC NEAR
    PUSHA
    MOV     BX,10    ;Will out in decimal.
    LEA     DI,Buf_Value
    MOV     AX,[Par1] 
    XOR     DX,DX
    CALL    lpSZ_ConvertTo_
    LEA     DX,Buf_Value
    CALL    OutLineLn
    
    MOV     BX,10
    LEA     DI,Buf_Value
    XOR     DX,DX
    MOV     AX,[Par2] 
    CALL    lpSZ_ConvertTo_
    LEA     DX,Buf_Value
    CALL    OutLineLn 
    POPA
    RETN
ENDP Func162_Mem

Func163_Stack PROC FAR 
    PUSH    BP
    MOV     BP,SP
    MOV     DX,[BP+10]
    CALL    OutLineLn  
    MOV     DX,[BP+8]
    CALL    OutLineLn
    MOV     DX,[BP+6]
    CALL    OutLineLn
    POP     BP
    RETF 6               
ENDP Func163_Stack	
		
OutLine: 
    ;INPUT: DX - ofsset to string.
    PUSH    AX
    MOV     AH,FUNC_OUT_STRING 
    INT     21h
    POP     AX
    RET     
OutLineLn:
    CALL    OutLine
    LEA     DX,Msg_CRLF
    CALL    OutLine
    RET  
;----------------------------------------------------------------------------
;----------------------extern W wStr_Len(lpSZ alpszStr)----------------------
;Returns length of the ASCIZ string alpszStr.
;IN:        ES:DI - alpszStr, AL - string terminator value
;OUT:       AX - Length of alpszStr
;Modifies:  AX CX
wStr_Len_:
    PUSH    DI
    MOV     CX,65535         ;Searching in full 16bit segment.
    CLD                      ;Increasing DI every scasb
    REPNE   SCASB            ;Searching...
    MOV     AX,65534         ;AX = full segment - zero value
    SUB     AX,CX            ;AX = length of string
    POP     DI
    RET                      
                    
;----------------------------------------------------------------------------
;-----------lpSZ_ConvertTo(lpV alpBuffer, SDW asdwValue, B abBase)-----------
;Converts asdwValue to its string representation in awBase count notation...
;...then puts result in alpBuffer and returns it as well.
;IN:        ES:DI - alpBuffer, DX:AX - asdwValue, BX - abBase
;OUT:       ES:DI - alpBuffer, or NULL of error.
;Modifies:  ES DI DX AX BX SI CX
lpSZ_ConvertTo_: 
    CMP     BX,2             ;Is abBase is less than 2?
    JB      Error            ;If so - exit with error.
    CMP     BX,36            ;Same for 36
    JA      Error
    PUSH    DS               ;Save old DS value
    PUSH    DI               ;...and DI, which will be modified late
    TEST    DH,80h           ;asdwValue is negative?
    JZ      PositiveOne      ;If no - deal with it like with positive.
    NEG     DX               ;In other case - make it positive
    NEG     AX
    SBB     DX,0
    MOV     CS:[bIsNegative],1h
                             ;^And set bIsNegative flag to 1.
  PositiveOne:
    ;PUSH    SEG szAlphabet 
    PUSH    CS
    POP     DS               ;DS = segment of szAlphabet
    MOV     SI,OFFSET szAlphabet    ;SI = offset of szAlphabet
  DivLoop:
    MOV     CX,AX            ;Saving low word of divident
    MOV     AX,DX            ;Will divide high word at first
    XOR     DX,DX            ;Clear remainder
    DIV     BX               ;Division
    XCHG    CX,AX            ;CX - high word of result...
                             ;...AX - low word of divident
    DIV     BX               ;Division on low word of divident.
    XCHG    DX,CX            ;DX - high word of result, CX - remainder
    ADD     SI,CX            ;Correct position in alphabet via remainder
    PUSH    AX               ;Save AX
    MOV     AL,BYTE DS:[SI]  ;AL = char from alphabet
    STOSB                    ;Append char to output buffer
    POP     AX               ;Restore low word of divident
    SUB     SI,CX            ;Restore start position of alphabet
    TEST    AX,AX            ;Is value became zero?
    JNZ     DivLoop          ;If no - continue converting
    CMP     CS:[bIsNegative],1
    JNE     DoNotInsertMinus ;Is value was negative? Jump if not.
    MOV     ES:[DI],'-'      ;Insert minus symbol otherwise.
    INC     DI               ;V...and clear IsNegative status.
    MOV     CS:[bIsNegative],0
  DoNotInsertMinus:
    STOSB                    ;Insert zero-terminator

    POP     DI               ;Restore DI - offset to buffer
    PUSH    DI               ;And saving it again
    MOV     SI,DI            ;SI = DI = start of the result buffer
    XOR     AL,AL
    CALL    wStr_Len_        ;Calculate length of result string
    ADD     SI,AX            ;SI = end of result string + 1
    SHR     AX,1             ;AX = AX / 2 (center of string)
    TEST    AX,AX            ;If AX = 0 then...
    JZ      AfterReverse     ;...There is no need to reverse 1 char.
  MoveLoop:
    DEC     SI               ;Moving to previous symbol (tail)
    MOV     BL,ES:[DI]       ;taking one char
    MOV     BH,ES:[SI]       ;And another one
    MOV     ES:[SI],BL       ;Reversing its places
    MOV     ES:[DI],BH
    INC     DI               ;moving to next symbol (head)
    DEC     AX               ;Iteration is gone
    JNZ     MoveLoop         ;If center is not reached - repeat
  AfterReverse:                
    MOV     ES:[DI + 2],'$'
    POP     DI               ;Restore offset of the result
    POP     DS               ;Restore original DS value
    RET
  Error:
    XOR     DI,DI            ;Returning NULL and exit.
    MOV     ES,DI  
    RET    
    bIsNegative DB 0         ;Is asdwValue negative. Default - no.
    szAlphabet  DB '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
;----------------------------------------------------------------------------
