#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void print_symbol();
void print_numbers();
void print_strings();

int global_variable1;
int global_variable2;

int main()
{
	char symbols[ 11 ];
	char symbol;
	symbols[ 0 ] = 'H';
	symbols[ 1 ] = 'E';
	symbols[ 2 ] = 'L';
	symbols[ 3 ] = 'L';
	symbols[ 4 ] = 'O';
	symbols[ 5 ] = ' ';
	symbols[ 6 ] = 'W';
	symbols[ 7 ] = 'O';
	symbols[ 8 ] = 'R';
	symbols[ 9 ] = 'L';
	symbols[ 10 ] = 'D';

	char string1[ 9 ] = "String 1$";
	char string2[ 9 ] = "String 2$";
	char string3[ 9 ] = "String 3$";

	// --- TASK 1 ---
	printf("Passes arguments to procedures in the register \n");

	for (int i = 0; i < 11; ++i)
	{
		symbol = symbols[ i ];

		asm 	mov 	dl, sumbol
		print_symbol();
	}
	printf("\n");

	// --- TASK 2 ---
	printf("Passes arguments to procedures in the memory \n");

	asm 	mov 	global_variable1, 13
	asm 	mov 	global_variable2, 666
	print_numbers();

	// --- TASK 3 ---
	printf("Passes arguments to procedures on the stack \n");

	asm 	lea 	ax,	string1[ 0 ]
	asm 	push 	ax

	asm 	lea 	ax,	string2[ 0 ]
	asm 	push 	ax

	asm 	lea 	ax,	string3[ 0 ]
	asm 	push 	ax

	print_strings();

	return 0;
}


void print_symbol()
{
	asm	mov 	ah, 06h
	asm	int 	21h
}

void print_numbers()
{
	char buffer[ 8 ];
	int length = 0;

	itoa(global_variable1, buffer, 10);
	length = strlen(buffer);
	buffer[ length ] = '$';

	asm 	lea 	dx, buffer[ 0 ]
	asm 	mov 	ah, 9
	asm 	int 	21h
	
	printf("\n");

	itoa(global_variable2, buffer, 10);
	length = strlen(buffer);
	buffer[ length ] = '$';

	asm 	lea 	dx, buffer[ 0 ]
	asm 	mov 	ah, 9
	asm 	int 	21h
	
	printf("\n");
}

void print_strings()
{
	asm 	push 	bp
    asm 	mov 	bp, sp

    asm 	mov 	dx, [ bp + 10 ]
    asm 	mov 	ah, 9
    asm 	int 	21h
	
	printf( "\n" );

	asm 	mov 	dx, [ bp + 8 ]
    asm 	mov 	ah, 9
    asm 	int 	21h

	printf( "\n" );

	asm 	mov 	dx, [ bp + 6 ]
    asm 	mov 	ah, 9
    asm 	int 	21h
    
    asm 	mov 	sp, bp
    asm 	pop 	bp
}