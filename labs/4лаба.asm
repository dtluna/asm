.model small
.stack 100h
.data
    messageT1 db "Task1.", 10, 13, '$'
    messageT2 db "Task2.", 10, 13, '$'
    messageT3 db "Task3.", 10, 13, '$'
    message1 db "String1: ", '$'
    message2 db "String2: ", '$'
    message3 db "Input symbol: ", '$'
    message4 db "String: ", '$'
    message5 db "Entered symbol was found ", '$'
    message6 db " times.", '$'
    message7 db "String1 contains string2!", '$'
    message8 db "String1 don't contains string2!", '$'
    string1 db "1234567890ABCDEFGHIJKLM", '$'   ; 23
    string2 db "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz34567890123456", '$' ; 66
    stringCmp db "1845654673527934566666666666666", '$' 
    string3 db "001840000", '$'   
    string4 db "000", '$'   
    buf db 5, 5 dup ('$')
    NewLine db 10,13, '$'
.code
start:   
    mov ax, @data
    mov ds, ax
    mov es, ax
    assume es:@data

; ----------------------     
    lea dx, messageT1
    mov ah, 9
    int 21h
    
    lea dx, message1
    int 21h
    
    lea dx, string1
    int 21h 
    
    lea dx, NewLine
    int 21h
    
    lea dx, message2
    int 21h
    
    lea dx, string2
    int 21h 
    
    lea dx, NewLine
    int 21h
    
    ; begin1 = 2
    ; begin2 = 1
    ; kol-vo = 18
    
    cld
    mov cx, 18
    mov si, offset string1[2]
    mov di, offset string2[1]
    
rep movsb
    
    lea dx, message1
    mov ah, 9
    int 21h
    
    lea dx, string1
    int 21h 
    
    lea dx, NewLine
    int 21h
    
    lea dx, message2
    int 21h
    
    lea dx, string2
    int 21h 
    
    lea dx, NewLine
    int 21h
; ----------------------    
    lea dx, messageT2
    int 21h 
    
    lea dx, message4
    int 21h
    
    lea dx, stringCmp
    int 21h
    
    lea dx, NewLine
    int 21h
    
    cld                         ;
    mov al, '$'                 ;
    mov bx, 0FFFFh              ;
    mov cx, 0FFFFh              ;
    mov di, offset stringCmp    ; find length of string
    repne scasb                 ; cx = length
    sub bx, cx                  ;
    mov cx, bx                  ;
    xor bx, bx                  ;
    
    lea dx, message3
    mov ah, 9
    int 21h
    
    mov ah, 1
    int 21h
    
    cld
    mov di, offset stringCmp
startFind:    
    repne scasb
    jcxz exitFind
    inc bx
    jmp startFind
    
exitFind:    
    
    lea dx, NewLine
    mov ah, 9
    int 21h
    
    lea dx, message5
    mov ah, 9
    int 21h
    
    mov ax, bx
    call numberAXtoString
    
    lea dx, buf
    mov ah, 9
    int 21h
    
    lea dx, message6
    int 21h
    
    lea dx, NewLine
    int 21h 
    
; ----------------------    
    lea dx, messageT3
    int 21h
    
    lea dx, message1
    int 21h
    
    lea dx, string3
    int 21h
    
    lea dx, NewLine
    int 21h
    
    lea dx, message2
    int 21h
    
    lea dx, string4
    int 21h
    
    lea dx, NewLine
    int 21h
    
    xor di, di 
    
    cld                         ;
    mov al, '$'                 ;
    mov bx, 0FFFFh              ;
    mov cx, 0FFFFh              ;
    mov di, offset string4      ; find length of string4
    repne scasb                 ; bx = length
    sub bx, cx                  ;
    dec bx                      ;
    
    push bx              
    
    cld                         ;
    mov al, '$'                 ;
    mov bx, 0FFFFh              ;
    mov cx, 0FFFFh              ;
    mov di, offset string3      ; find length of string3
    repne scasb                 ; cx = length + 1
    sub bx, cx                  ;
    mov cx, bx                  ;
    xor bx, bx                  ; 
        
    pop bx  
    
    mov di, offset string3      ;
continueFind1:                  ;
    mov al, string4[0]          ;
                                ;
repne scasb                     ;
    jcxz notContain             ;
    push cx                     ;
    push di                     ; find string4 in string3
    mov cx, bx                  ;
    mov si, offset string4[1]   ;
    repe cmpsb                  ;
        jcxz contain            ;
        pop di                  ;
        pop cx                  ;
        jmp continueFind1:      ;
        
contain:
    lea dx, message7
    mov ah, 9
    int 21h
    jmp endOfProgramm
notContain:
    lea dx, message8
    int 21h
    mov ah, 9
    
endOfProgramm:                          
    mov ax, 4C00h
    int 21h



numberAXtoString:
    push di
    push si
    push ax
    xor di, di
    xor si, si
    cmp ax, 0
        jge loopPositive
    inc di
    neg ax
    mov buf[si], 45
    inc si          ; first symbol '-'
loopPositive:
    add di, 4
    mov bx, 10
loopN:
    dec di
    xor dx, dx    
    div bx
    add dl, 48
    mov buf[di], dl
    cmp al, 0
        jne loopN
loopSdvig:
    cmp si, di
        je endloopN
    mov dl, buf[di]         
    mov buf[si], dl
    inc di
    inc si
    cmp di, 5
        jne loopSdvig
fillbufS:        
    mov buf[si], '$'
    inc si
    cmp si, 5
        jle fillbufS     
endloopN:
    pop ax
    pop si
    pop di
    ret
         
end start