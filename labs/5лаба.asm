.model small
.stack 100h
.data
    messageT1 db "Task1.", 10, 13, '$'
    messageT2 db "Task2.", 10, 13, '$'
    messageT3 db "Task3.", 10, 13, '$'
    NewLine db 10,13, '$'
    
    string1 db "12345", '$'
    string2 db "67890", '$'
    string3 db "qwert", '$'    
      
    buf db 5, 5 dup ('$')  
    
    number1 dw 1000
    number2 dw 5403 
    
    off_procSymbols dw ?
    seg_procSymbols dw ? 
    
    off_procStrings dw ?
    seg_procStrings dw ?
    
.code

start:  
    mov ax, @data                
    mov ds, ax
    
    lea dx, messageT1
    mov ah, 9
    int 21h
    
    mov off_procSymbols, procSymbols
    mov seg_procSymbols, cs
    
    mov al, '5'
    call far off_procSymbols
   
    mov al, '4'
    call far off_procSymbols
    
    mov al, '3'
    call far off_procSymbols
    
    mov al, '2'
    call far off_procSymbols
    
    mov al, '1'
    call far off_procSymbols
    
    lea dx, NewLine
    mov ah, 9
    int 21h
    
    lea dx, messageT2
    int 21h
    
    call procNumbers
    
    lea dx, NewLine
    mov ah, 9
    int 21h
    
    lea dx, messageT3
    int 21h
    
    xor ax, ax
    
    mov ax, offset string1 
    push ax
    mov ax, offset string2 
    push ax
    mov ax, offset string3 
    push ax
    
    mov off_procStrings, procStrings
    mov seg_procStrings, cs
     
    call far off_procStrings 
    
    mov ax, 4C00h 
    int 21h    
      
procSymbols proc far

    mov dl, al
    mov ah, 6
    int 21h
    
    retf
procSymbols endp


procNumbers proc near
    push ax
    push dx
    
    mov ax, number1
    call numberAXtoString
    lea dx, buf
    mov ah, 9
    int 21h
    
    lea dx, NewLine
    mov ah, 9
    int 21h
    
    mov ax, number2
    call numberAXtoString
    lea dx, buf
    mov ah, 9
    int 21h
    
    pop dx
    pop ax
    ret
procNumbers endp


procStrings proc far
    push bp
    mov bp, sp
    
    mov dx, [bp+10]
    mov ah, 9
    int 21h
    
    lea dx, NewLine
    mov ah, 9
    int 21h
    
    mov dx, [bp+8]
    mov ah, 9
    int 21h
    
    lea dx, NewLine
    mov ah, 9
    int 21h
    
    mov dx, [bp+6]
    mov ah, 9
    int 21h
    
    lea dx, NewLine
    mov ah, 9
    int 21h
    
    mov sp, bp
    pop bp
    retf 6
procStrings endp


numberAXtoString proc 
    push di
    push si
    push ax
    xor di, di
    xor si, si
    cmp ax, 0
        jge loopPositive
    inc di
    neg ax
    mov buf[si], 45
    inc si          ; first symbol '-'
loopPositive:
    add di, 4
    mov bx, 10
loopN:
    dec di
    xor dx, dx    
    div bx
    add dl, 48
    mov buf[di], dl
    cmp al, 0
        jne loopN
loopSdvig:
    cmp si, di
        je endloopN
    mov dl, buf[di]         
    mov buf[si], dl
    inc di
    inc si
    cmp di, 5
        jne loopSdvig
fillbufS:        
    mov buf[si], '$'
    inc si
    cmp si, 5
        jle fillbufS     
endloopN:
    pop ax
    pop si
    pop di
    ret
numberAXtoString endp

end start